package docuphase

import (
	"encoding/json"
	"fmt"
	"strings"
)

// IndexQuery for querying cabinets
// Available operators
// contains
// endswith
// startswith
// isblank
// <
// <=
// >
// >=
// equalto
type IndexQuery struct {
	Id       int    `json:"id,omitempty"`
	Index    string `json:"index"`
	Value    string `json:"value,omitempty"`
	Operator string `json:"operator,omitempty"`
}

// Indices slice of indexes
type Indices []Index

// Index for many things
type Index struct {
	Index string `json:"index"`
	Value string `json:"value"` // optional?
}

// IndexValue Find the value for a given index, or empty string
func (f *Folder) IndexValue(index string) (value string) {
	for _, v := range f.Indices {
		if v.Index == index {
			return v.Value
		}
	}
	return ""
}

// ToMap slice of Indices to map of indexes with values
func (wrs Indices) ToMap() (m map[string]string) {
	m = map[string]string{}
	for i := range wrs {
		m[strings.ToLower(wrs[i].Index)] = wrs[i].Value
	}
	return m
}

// ToJSON marshall indexes to json for the purpose of unmarshalling
func (wrs Indices) ToJSON() ([]byte, error) {
	m := wrs.ToMap()
	b, err := json.Marshal(m)
	if err != nil {
		return nil, fmt.Errorf("problem unwrapping %v: %w", wrs, err)
	}
	return b, nil
}

// Unmarshal recently marshalled json to a thing
func (wrs Indices) Unmarshal(v interface{}) error {
	b, err := wrs.ToJSON()
	if err != nil {
		return err
	}
	return json.Unmarshal(b, v)
}
