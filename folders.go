package docuphase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// FoldersResponse GetFolders response
type FoldersResponse struct {
	Folders      Folders `json:"folders"`
	HasError     bool    `json:"hasError"`
	ErrorMessage string  `json:"errorMessage"`
}

// FolderAddResponse Response to add folder to cabinet
type FolderAddResponse struct {
	Folder       Folder `json:"folder"`
	HasError     bool   `json:"hasError"`
	ErrorMessage string `json:"errorMessage"`
}

// Folders slice of folders
type Folders []Folder

// Folder represents a folder in docuphase
type Folder struct {
	FolderID   int     `json:"folderId"`  // optional
	CabinetID  int     `json:"cabinetId"` // optional, this or cabinet might be required
	Department string  `json:"department"`
	Cabinet    string  `json:"cabinet"` // optional, this or cabinetId might be required
	Indices    Indices `json:"indices"`
}

type FolderDeleteRequest struct {
	FolderID   int    `json:"folderId"` // optional
	Department string `json:"department"`
	Cabinet    string `json:"cabinet"` // optional, this or cabinetId might be required
	Force      bool   `json:"force,omitempty"`
}

type FolderDeleteResponse struct {
	Folder       FolderDeleteRequest `json:"folder"`
	HasError     bool                `json:"hasError"`
	ErrorMessage string              `json:"errorMessage"`
}

// type Indices map[string]string

// func (ind Indices) UnmarshalJSON(data []byte) error {
// 	if ind == nil {
// 		ind = make(Indices)
// 	}
// 	v := &IndicesRaw{}
// 	err := json.Unmarshal(data, v)
// 	if err != nil {
// 		return err
// 	}
// 	for _, r := range *v {
// 		ind[r.Index] = r.Value
// 	}
// 	return nil
// }

// func (ind Indices) MarshalToBadJSON() (data []byte, err error) {
// 	v := indicesRaw{}
// 	for index, value := range ind {
// 		v = append(v, indexRaw{Index: index, Value: value})
// 	}
// 	return json.Marshal(v)
// }

// type IndicesRaw []indexRaw
// type indexRaw struct {
// 	Index string `json:"index"`
// 	Value string `json:"value"`
// }
// type IndicesRaw []struct {
// 	Index string `json:"index"`
// 	Value string `json:"value"`
// }

// // Find the value for a given index, or empty string
// func (f *Folder) IndexValue(index string) (value string) {
// 	for _, v := range f.Indices {
// 		if v.Index == index {
// 			return v.Value
// 		}
// 	}
// 	return ""
// }

// GetFolders return list of folders in a cabinet
func (client *Client) GetFolders(department string, cabinet string) (f Folders, err error) {

	url := fmt.Sprintf("/folders/%s/%s", department, cabinet)
	req, err := client.NewAuthRequest(url, "GET", nil)
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("http error %v", resp.StatusCode)
	}

	res := FoldersResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&res)
	if err != nil {
		return nil, err
	}
	if res.HasError != false {
		return nil, fmt.Errorf("get folders error  %v", res.ErrorMessage)
	}
	return res.Folders, nil
}

// AddFolder Add a folder
func (client *Client) AddFolder(f Folder) (folder Folder, err error) {
	url := fmt.Sprintf("/folders/add")

	jsonString, err := json.Marshal(f)
	if err != nil {
		panic(err)
	}
	req, err := client.NewAuthRequest(url, "POST", bytes.NewBuffer(jsonString))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return folder, fmt.Errorf("http error %v", resp.StatusCode)
	}

	res := FolderAddResponse{}

	bod, _ := ioutil.ReadAll(resp.Body)

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&res)
	if err != nil {
		return folder, err
	}
	if res.HasError != false {
		return folder, fmt.Errorf("add folder error:  %v", res.ErrorMessage)
	}
	return res.Folder, nil

}

// DeleteFolder delete a folder from a cabinet
func (client *Client) DeleteFolder(folderToDelete FolderDeleteRequest) (response FolderDeleteResponse, err error) {
	url := fmt.Sprintf("/folders/delete")
	jsonString, err := json.Marshal(folderToDelete)

	req, err := client.NewAuthRequest(url, http.MethodPost, bytes.NewBuffer(jsonString))

	req.Header.Set("Content-Type", "application/json")
	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return response, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, _ := ioutil.ReadAll(resp.Body)

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&response)
	if err != nil {
		return
	}
	if response.HasError != false {
		return response, fmt.Errorf("error deleting folder %v", response.ErrorMessage)
	}

	return
}

type FolderEditRequest struct {
	DocId      int     `json:"DocId"`
	Department string  `json:"Department"`
	Cabinet    string  `json:"Cabinet"`
	Indices    Indices `json:"Indices"`
}

type FolderEditResponse struct {
	HasError     bool   `json:"HasError"`
	ErrorMessage string `json:"ErrorMessage"`
	IsSuccess    bool   `json:"IsSuccess"`
	Folder       struct {
		DocId      int    `json:"DocId"`
		Department string `json:"Department"`
		Cabinet    string `json:"Cabinet"`
		Indices    []struct {
			Index string `json:"Index"`
			Value string `json:"Value"`
		} `json:"Indices"`
	} `json:"Folder"`
}

// EditFolder edits a folder in the thing
func (client *Client) EditFolder(request FolderEditRequest) (response FolderEditResponse, err error) {

	url := fmt.Sprintf("/folders/edit")
	jsonString, err := json.Marshal(request)

	req, err := client.NewAuthRequest(url, http.MethodPost, bytes.NewBuffer(jsonString))

	req.Header.Set("Content-Type", "application/json")
	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return response, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, _ := ioutil.ReadAll(resp.Body)

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&response)
	if err != nil {
		return
	}
	if response.HasError != false {
		return response, fmt.Errorf("error editing folder %v", response.ErrorMessage)
	}

	return
}
