package docuphase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// File represents a file in docuphase
type File struct {
	FileID       int    `json:"fileId"`
	FileFullName string `json:"fileFullName"`
	Department   string `json:"department"`
	Cabinet      string `json:"cabinet"`
	FolderID     int    `json:"folderId"`
	TabID        int    `json:"tabId"`
}

// FilesAddResponse Response containing any errors and the file that was added
type FilesAddResponse struct {
	File         File   `json:"file"`
	HasError     bool   `json:"hasError"`
	ErrorMessage string `json:"errorMessage"`
}

// FilesAddRequest TODO seems like it's not finished
type FilesAddRequest struct {
}

// FilesUploadToFolderRequest request for file upload
type FilesUploadToFolderRequest struct {
	Department  string `json:"department"`
	CabinetID   int    `json:"cabinetId"`
	DocID       int    `json:"docId"`
	TabID       int    `json:"tabId,omitempty"`
	Filename    string `json:"filename"`
	FileContent []byte `json:"fileContent"`
}

// FilesUploadToFolderResponse Response containing any errors and the initial request
type FilesUploadToFolderResponse struct {
	Upload       FilesUploadToFolderRequest `json:"upload"`
	FileID       int                        `json:"fileId"`
	HasError     bool                       `json:"hasError"`
	ErrorMessage string                     `json:"errorMessage"`
}

// FilesDownloadRequest request for file download
type FilesDownloadRequest struct {
	Department string `json:"department"`
	Cabinet    string `json:"cabinet"`
	FileIds    []int  `json:"fileIds"`
	Format     string `json:"format"`   // optional
	Filename   string `json:"filename"` // optional
}

// FilesDownloadResponse Response containing any errors, the file contents and the request
type FilesDownloadResponse struct {
	DownloadFile FilesDownloadRequest `json:"downloadFile"`
	FileContent  []byte               `json:"fileContent"`
	HasError     bool                 `json:"hasError"`
	ErrorMessage string               `json:"errorMessage"`
}

// FilesGetFilesInFolderResponse Response containing any errors and the files in the folder
type FilesGetFilesInFolderResponse struct {
	Files        FilesInFolder `json:"files"`
	HasError     bool          `json:"hasError"`
	ErrorMessage string        `json:"errorMessage"`
}

// FilesInFolder slice of filenames and ids
type FilesInFolder []struct {
	FileID       int      `json:"fileId"`
	Filename     string   `json:"filename"`
	Location     string   `json:"location"`
	Subfolder    string   `json:"subfolder"`
	CreatedBy    string   `json:"createdBy"`
	CreationDate DateTime `json:"creationDate"`
	FileSize     int      `json:"fileSize"`
	Ordering     int      `json:"ordering"`
}

// FilesUploadToFolder Upload a file to a folder
func (client *Client) FilesUploadToFolder(file FilesUploadToFolderRequest) (fileId int, err error) {
	url := fmt.Sprintf("/files/uploadtofolder")

	b := &[]byte{}
	buf := bytes.NewBuffer(*b)

	err = json.NewEncoder(buf).Encode(file)
	if err != nil {
		return fileId, fmt.Errorf("file encode err: %v", err)
	}

	req, err := client.NewAuthRequest(url, "POST", buf)
	if err != nil {
		return fileId, fmt.Errorf("new request err: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.c.Do(req)
	if err != nil {
		return fileId, fmt.Errorf("do request err: %v", err)
	}

	res := FilesUploadToFolderResponse{}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&res)
	if err != nil {
		bod, _ := ioutil.ReadAll(resp.Body)
		return fileId, fmt.Errorf("decode response err: %v \nBODY:%v", err, bod)
	}
	if res.HasError != false {
		return fileId, fmt.Errorf("files upload to folder error:  %v", res.ErrorMessage)
	}

	return res.FileID, nil
}

// FilesGetFilesInFolder
// TODO NOT TESTED
// department real name
// cabinet real name
func (client *Client) FilesGetFilesInFolder(department string, cabinet string, folderId int) (files FilesInFolder, err error) {
	url := fmt.Sprintf("/files/%v/%v/%v/false", department, cabinet, folderId)

	req, err := client.NewAuthRequest(url, "GET", bytes.NewBuffer([]byte{}))
	if err != nil {
		return files, err
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return files, err
	}
	if resp.StatusCode != http.StatusOK {
		return files, fmt.Errorf("http error %v", resp.StatusCode)
	}

	FilesInFolderResponse := FilesGetFilesInFolderResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&FilesInFolderResponse)
	if err != nil {
		return nil, err
	}
	return FilesInFolderResponse.Files, nil

}

// FilesDownload download a file
// TODO NOT TESTED
func (client *Client) FilesDownload(filesDownload FilesDownloadRequest) (file FilesDownloadResponse, err error) {
	url := fmt.Sprintf("/files/download")

	b := &[]byte{}
	buf := bytes.NewBuffer(*b)

	err = json.NewEncoder(buf).Encode(filesDownload)
	if err != nil {
		return file, fmt.Errorf("file encode err: %v", err)
	}

	req, err := client.NewAuthRequest(url, "POST", buf)
	if err != nil {
		return file, fmt.Errorf("new request err: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.c.Do(req)
	if err != nil {
		return file, fmt.Errorf("do request err: %v", err)
	}

	res := FilesDownloadResponse{}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&res)
	if err != nil {
		return file, fmt.Errorf("decode response err: %v", err)
	}
	if res.HasError != false {
		return file, fmt.Errorf("files download error:  %v", res.ErrorMessage)
	}

	return res, nil
}

// FilesAdd Adds a selected file to specific subfolder/tab
// TODO NOT IMPLEMENTED
// and also not needed maybe?
func (client *Client) FilesAdd(file FilesAddRequest) (err error) {

	return fmt.Errorf("NOT IMPLEMENTED")

	// url := fmt.Sprintf("files/add")
	//
	// b := &[]byte{}
	// buf := bytes.NewBuffer(*b)
	//
	// json.NewEncoder(buf).Encode(file)
	//
	// req, err := authRequest(*client, url, "POST", buf)
	// if err != nil {
	//	return err
	// }
	//
	// resp, err := client.c.Do(req)
	// if err != nil {
	//	return err
	// }
	// if resp.StatusCode != http.StatusOK {
	//	return fmt.Errorf("http error %v", resp.StatusCode)
	// }
	//
	// res := FilesAddResponse{}
	//
	// decoder := json.NewDecoder(resp.Body)
	// err = decoder.Decode(&res)
	// if err != nil {
	//	return err
	// }
	// if res.HasError != false {
	//	return fmt.Errorf("files add error  %v", res.ErrorMessage)
	// }
	//
	// return nil
}
