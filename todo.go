package docuphase

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// GetToDoResponse response from server for to do items for user
type GetToDoResponse struct {
	ToDoList     ToDoList `json:"toDoList"`
	HasError     bool     `json:"hasError"`
	ErrorMessage string   `json:"errorMessage"`
}

// ToDoList slice of ToDoItem
type ToDoList []ToDoItem

// ToDoItem an individual to do item
type ToDoItem struct {
	Id                 int    `json:"id"`
	Department         string `json:"department"`
	Cabinet            string `json:"cabinet"`
	DocId              int    `json:"docId"`
	FileId             int    `json:"fileId"`
	WorkflowNodeId     int    `json:"workflowNodeId"`
	Username           string `json:"username"`
	WorkflowDefId      int    `json:"workflowDefId"`
	WorkflowDocumentId int    `json:"workflowDocumentId"`
	Priority           int    `json:"priority"`
	Notes              string `json:"notes,omitempty"`
	Date               string `json:"date"`
	DelegatedFrom      string `json:"delegatedFrom,omitempty"`
}

// GetUserToDo gets the to do items for a username
func (client *Client) GetUserToDo(username string) (result GetToDoResponse, err error) {

	url := fmt.Sprintf("/todo/%v/user/%v", client.DefaultDepartment, username)
	req, err := client.NewAuthRequest(url, http.MethodGet, nil)
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return result, fmt.Errorf("error making request %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return result, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, fmt.Errorf("error reading response body, %v", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&result)
	if err != nil {
		return result, fmt.Errorf("error decoding response, %v", err)
	}

	return result, nil
}

// GetUserToDoWithContext gets the to do items for a username
func (client *Client) GetUserToDoWithContext(ctx context.Context, username string) (result GetToDoResponse, err error) {

	url := fmt.Sprintf("/todo/%v/user/%v", client.DefaultDepartment, username)
	req, err := client.NewAuthRequestWithContext(ctx, url, http.MethodGet, nil)
	if err != nil {
		return
	}

	resp, err := client.makeRequestWithContext(ctx, req)

	if err != nil {
		return result, fmt.Errorf("error making request %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return result, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, fmt.Errorf("error reading response body, %v", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&result)
	if err != nil {
		return result, fmt.Errorf("error decoding response, %v", err)
	}

	return result, nil
}
