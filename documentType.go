package docuphase

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// DocumentTypesResponse Response containing any errors and the DocumentTypes
type DocumentTypesResponse struct {
	DocumentTypes DocumentTypes `json:"documentTypes"`
	HasError      bool          `json:"hasError"`
	ErrorMessage  string        `json:"errorMessage"`
}

// DocumentTypes in a given department and cabinet
type DocumentTypes []struct {
	DocumentTypeID      int    `json:"documentTypeID"`
	DocumentTypeEnabled int    `json:"documentTypeEnabled"`
	InternalName        string `json:"internalName"`
	DisplayName         string `json:"displayName"`
	Indices             []struct {
		ID    int    `json:"id"`
		Index string `json:"index"`
		Value string `json:"value"`
	} `json:"indices"`
	DocTypeIndexDefs []struct {
		IndexName      string   `json:"indexName"`
		DefinitionList []string `json:"definitionList"`
	} `json:"docTypeIndexDefs"`
}

// GetDocumentTypes Get a list of document types for a cabinet in a department
// TODO UNTESTED
func (client *Client) GetDocumentTypes(department, cabinet string) (dTypes DocumentTypes, err error) {

	url := fmt.Sprintf("/documenttypes/%v/%v", department, cabinet)

	req, err := client.NewAuthRequest(url, "GET", nil)
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return dTypes, fmt.Errorf("http error %v", resp.StatusCode)
	}

	dtr := DocumentTypesResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&dtr)
	if err != nil {
		return nil, err
	}
	return dtr.DocumentTypes, nil
}
