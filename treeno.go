package docuphase

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"golang.org/x/oauth2"
)

// Config Configs required to create a client
type Config struct {
	Host     string
	Username string
	Email    string
	Password string
}

// NewService Create a new service with the config, and set a default department
func NewService(conf *Config, deptDefault string) (client *Client, err error) {
	oauthClient := oauth2.NewClient(context.Background(), oauth2.ReuseTokenSource(nil, conf))

	client = &Client{
		c:                 oauthClient,
		DefaultDepartment: deptDefault,
	}

	client.baseUrl = fmt.Sprintf("%v/api", conf.Host)

	return
}

// Authenticate authenticates to the docuphase API
func (conf *Config) Authenticate() (*Authentication, error) {
	client := &http.Client{}
	url := fmt.Sprintf("%v/api/authentication/login2", conf.Host)
	a := User{
		Username: conf.Username,
		Password: conf.Password,
		Email:    conf.Email,
	}

	j, err := json.Marshal(a)
	if err != nil {
		return nil, fmt.Errorf("docuphase Client Authentication Failure marshalling request: %w", err)
	}

	authReq, err := http.NewRequest("POST", url, bytes.NewBuffer(j))
	if err != nil {
		return nil, fmt.Errorf("docuphase Client Authentication Failure Making POST login2 request: %w", err)
	}
	authReq.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(authReq)
	if err != nil {
		return nil, fmt.Errorf("docuphase Client Authentication Failure doing POST: %w", err)
	}

	var bodyBytes []byte
	if resp.StatusCode != http.StatusOK {
		bodyBytes, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, fmt.Errorf("docuphase Client Authentication Failure Reading resp Body: %w", err)
		}
		return nil, fmt.Errorf("docuphase Client Authentication Failure, Not OK: status %v, response: %q", resp.StatusCode, string(bodyBytes))
	}

	decoder := json.NewDecoder(resp.Body)
	ar := AuthResp{}
	err = decoder.Decode(&ar)
	// log.Printf("\n%+v\n%+v\n\n", ar, err)
	if err != nil {
		return nil, fmt.Errorf("docuphase Client Authentication Failure decoding Response: %w", err)
	}
	if ar.HasError != false {
		return nil, fmt.Errorf("docuphase Client Authentication Failure, Has error: %v", ar.ErrorMessage)
	}
	return &ar.Authentication, nil
}

// NewAuthRequest with auth header and body
func (client *Client) NewAuthRequest(path string, method string, body io.Reader) (req *http.Request, err error) {
	req, err = http.NewRequest(method, client.baseUrl+path, body)
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	return
}

// NewAuthRequestWithContext with auth header and body and context
func (client *Client) NewAuthRequestWithContext(ctx context.Context, path string, method string, body io.Reader) (req *http.Request, err error) {
	req, err = http.NewRequestWithContext(ctx, method, client.baseUrl+path, body)
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	return
}

// Token Authenticates and creates a new oauth token
func (conf *Config) Token() (*oauth2.Token, error) {
	au, err := conf.Authenticate()
	if err != nil {
		return nil, err
	}
	t := &oauth2.Token{
		AccessToken:  au.Token,
		TokenType:    "bearer",
		RefreshToken: au.RefreshToken,
		Expiry:       time.Time(au.Expiration),
	}
	t.WithExtra(map[string]interface{}{"User": au.User})
	return t, nil
}

// ErrNotImplemented this method isn't implemented yet
var ErrNotImplemented = fmt.Errorf("not implemented")

// GetServerResponse by doing an auth request
func (client *Client) GetServerResponse(url string, method string, body io.Reader) (resp *http.Response, err error) {
	ar, err := client.NewAuthRequest(url, method, body)
	if err != nil {
		return
	}
	resp, err = client.c.Do(ar)
	if err != nil {
		return
	}
	return
}

func (client *Client) makeRequestWithContext(ctx context.Context, req *http.Request) (*http.Response, error) {
	finish := make(chan struct{})
	var resp *http.Response
	var err error

	go func(req *http.Request) {
		resp, err = client.c.Do(req)
		finish <- struct{}{}
	}(req)

	select {
	case <-finish:
		return resp, err
	case <-ctx.Done():
		return nil, fmt.Errorf("context cancelled or something: %v", ctx.Err())
	}
}
