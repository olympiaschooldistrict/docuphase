package docuphase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type PersonalInboxInfoResponse struct {
	Info         []PersonalInboxInfo `json:"info"`
	HasError     bool                `json:"hasError"`
	ErrorMessage string              `json:"errorMessage"`
}

type PersonalInboxInfo struct {
	Username string `json:"username"`
	Value    string `json:"value"`
}

func (client *Client) GetPersonalInboxInfo() (result PersonalInboxInfoResponse, err error) {
	url := fmt.Sprintf("/inbox/personal/info/%v", client.DefaultDepartment)

	req, err := client.NewAuthRequest(url, "GET", nil)
	if err != nil {
		return result, fmt.Errorf("error building request %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := client.c.Do(req)
	if err != nil {
		return result, fmt.Errorf("error making request %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return result, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, fmt.Errorf("error reading response body, %v", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&result)
	if err != nil {
		return result, fmt.Errorf("error decoding response, %v", err)
	}

	return result, nil

}
