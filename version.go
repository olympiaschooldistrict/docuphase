package docuphase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type CheckOutFile struct {
	Department string `json:"Department"`
	Cabinet    string `json:"Cabinet"`
	FileId     int    `json:"FileId"`
}

type VersionFileCheckOutRequest CheckOutFile

type VersionFileCheckOutResponse struct {
	HasError     bool         `json:"HasError"`
	ErrorMessage string       `json:"ErrorMessage"`
	Content      []byte       `json:"Content"`
	CheckOutFile CheckOutFile `json:"CheckOutFile"`
}

// VersionsFileCheckOut check out a file
func (client *Client) VersionsFileCheckOut(request VersionFileCheckOutRequest) (response VersionFileCheckOutResponse, err error) {

	url := fmt.Sprintf("/versions/files/checkout")

	reqBody, err := json.Marshal(request)
	if err != nil {
		return
	}

	req, err := client.NewAuthRequest(url, "POST", bytes.NewReader(reqBody))
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		return response, fmt.Errorf("http error %v", resp.StatusCode)
	}

	response = VersionFileCheckOutResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&response)
	if err != nil {
		return response, err
	}

	if response.HasError != false {
		return response, fmt.Errorf("get cabinet error  %v", response.ErrorMessage)
	}

	return response, nil
}

type CheckIn struct {
	Department  string `json:"Department"`
	Cabinet     string `json:"Cabinet"`
	FileId      int    `json:"FileId"`
	FileName    string `json:"FileName"`
	EncFileData []byte `json:"EncFileData"`
}

// CheckInRequest request for file upload
type CheckInRequest CheckIn

// CheckInResponse Response containing any errors and the initial request
type CheckInResponse struct {
	HasError     bool    `json:"HasError"`
	ErrorMessage string  `json:"ErrorMessage"`
	Data         string  `json:"Data"`
	CheckIn      CheckIn `json:"CheckIn"`
}

// VersionsFileCheckIn Check In a file I guess
func (client *Client) VersionsFileCheckIn(request CheckInRequest) (response CheckInResponse, err error) {
	url := fmt.Sprintf("/versions/files/checkin")

	buf := new(bytes.Buffer)

	err = json.NewEncoder(buf).Encode(request)
	if err != nil {
		return response, fmt.Errorf("file encode err: %v", err)
	}

	req, err := client.NewAuthRequest(url, "POST", buf)
	if err != nil {
		return response, fmt.Errorf("new request err: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.c.Do(req)
	if err != nil {
		return response, fmt.Errorf("do request err: %v", err)
	}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&response)
	if err != nil {
		bod, _ := ioutil.ReadAll(resp.Body)
		return response, fmt.Errorf("decode response err: %v \nBODY:%v", err, bod)
	}
	if response.HasError != false {
		return response, fmt.Errorf("files upload to folder error:  %v", response.ErrorMessage)
	}

	return response, nil
}
