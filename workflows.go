package docuphase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Workflow represents a docuphase workflow
type Workflow struct {
	Department string `json:"department"`
	Cabinet    string `json:"cabinet"`
	DocId      int    `json:"docId"`
	FileId     int    `json:"fileId"`
	OwnerName  string `json:"ownerName"`
	WfId       int    `json:"wfId"`
	Subfolder  string `json:"subfolder,omitempty"`
}

// StartWorkflowResponse server response for starting a workflow
type StartWorkflowResponse struct {
	IsSuccess    bool                `json:"isSuccess"`
	Result       StartWorkflowResult `json:"result"`
	HasError     bool                `json:"hasError"`
	ErrorMessage string              `json:"errorMessage"`
}

// StartWorkflowResult results of starting a workflow
type StartWorkflowResult struct {
	Success   bool     `json:"success"`
	ToDoUsers []string `json:"toDoUsers"`
	WfDocId   int      `json:"wfDocId"`
	NodeId    int      `json:"nodeId"`
}

// StartWorkflow from a workflow request
func (client *Client) StartWorkflow(w Workflow) (result StartWorkflowResponse, err error) {
	url := fmt.Sprintf("/workflow/start")

	jsonString, err := json.Marshal(w)
	if err != nil {
		return result, fmt.Errorf("marshalling data, %v", err)
	}

	req, err := client.NewAuthRequest(url, "POST", bytes.NewBuffer(jsonString))
	if err != nil {
		return result, fmt.Errorf("error building request %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := client.c.Do(req)
	if err != nil {
		return result, fmt.Errorf("error making request %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return result, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, fmt.Errorf("error reading response body, %v", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&result)
	if err != nil {
		return result, fmt.Errorf("error decoding response, %v", err)
	}

	return result, nil

}

// WorkflowDefinitionsResponse Response from getting definitions of workflows
type WorkflowDefinitionsResponse struct {
	WorkflowDefinitions []WorkflowDefinition `json:"workflowDefinitions"`
	HasError            bool                 `json:"hasError"`
	ErrorMessage        string               `json:"errorMessage"`
}

// WorkflowDefinition Workflow id and description
type WorkflowDefinition struct {
	WorkflowId   int    `json:"workflowId"`
	WorkflowName string `json:"workflowName"`
}

// GetWorkflowDefinitions gets a list of available workflow ids and names
func (client *Client) GetWorkflowDefinitions() (result WorkflowDefinitionsResponse, err error) {
	url := fmt.Sprintf("/workflowdefs/%v", client.DefaultDepartment)

	req, err := client.NewAuthRequest(url, "GET", nil)
	if err != nil {
		return result, fmt.Errorf("error building request %v", err)
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return result, fmt.Errorf("error making request %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return result, fmt.Errorf("http error %v", resp.StatusCode)
	}

	bod, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, fmt.Errorf("error reading response body, %v", err)
	}

	decoder := json.NewDecoder(bytes.NewReader(bod))
	err = decoder.Decode(&result)
	if err != nil {
		return result, fmt.Errorf("error decoding response, %v", err)
	}

	return result, nil

}
