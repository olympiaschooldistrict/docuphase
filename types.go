package docuphase

import (
	"fmt"
	"strings"
	"time"
)

// DateFormat format used by docuphase
const DateFormat = "01/02/2006"
const DateTimeFormat = "2006-01-02T15:04:05"

// Date Valid docuphase date
type Date struct {
	time.Time
}

type DateTime struct {
	time.Time
}

// MarshalJSON  Marshall to valid docuphase date
func (d *Date) MarshalJSON() ([]byte, error) {

	if d.Time.IsZero() {
		return []byte("\"\""), nil
	}
	return []byte("\"" + d.Format(DateFormat) + "\""), nil
}

// UnmarshalJSON Unmarshall from docuphase time to go time
func (d *Date) UnmarshalJSON(data []byte) error {
	var t time.Time

	stripped := strings.TrimPrefix(strings.TrimSuffix(string(data), "\""), "\"")

	if len(stripped) == 0 {
		return nil
	}
	t, err := time.Parse(DateFormat, stripped)
	if err != nil {
		return err
	}

	d.Time = t
	return nil
}

// MarshalJSON  Marshall to valid docuphase date
func (d *DateTime) MarshalJSON() ([]byte, error) {

	if d.Time.IsZero() {
		return []byte("\"\""), nil
	}
	return []byte("\"" + d.Format(DateTimeFormat) + "\""), nil
}

// UnmarshalJSON Unmarshall from docuphase time to go time
func (d *DateTime) UnmarshalJSON(data []byte) error {
	var t time.Time

	stripped := strings.TrimPrefix(strings.TrimSuffix(string(data), "\""), "\"")

	if len(stripped) == 0 {
		return nil
	}
	t, err := time.Parse(DateTimeFormat, stripped)
	if err != nil {
		return err
	}

	d.Time = t
	return nil
}

// Bool docuphase representation of a bool, like "True", "Yes", etc.
type Bool bool

// MarshalJSON  Marshall to valid docuphase date
func (b *Bool) MarshalJSON() ([]byte, error) {

	if *b {
		return []byte("\"true\""), nil
	} else {
		return []byte("\"false\""), nil
	}
}

// ToString  Marshall to valid docuphase date
func (b *Bool) ToString() string {

	if *b {
		return "true"
	} else {
		return "false"
	}
}

// UnmarshalJSON Unmarshall from docuphase some kind of positive representation to go bool
func (b *Bool) UnmarshalJSON(data []byte) error {

	stripped := strings.TrimPrefix(strings.TrimSuffix(string(data), "\""), "\"")

	if strings.ToLower(stripped) == "true" {
		*b = true
		return nil
	} else if strings.ToLower(stripped) == "false" {
		*b = false
		return nil
	}

	if strings.ToLower(stripped) == "yes" {
		*b = true
		return nil
	} else if strings.ToLower(stripped) == "no" {
		*b = false
		return nil
	}

	if strings.ToLower(stripped) == "1" {
		*b = true
		return nil
	} else if strings.ToLower(stripped) == "0" {
		*b = false
		return nil
	}

	// if it's blank it's false, like an empty bool
	if stripped == "" {
		*b = false
		return nil
	}

	return fmt.Errorf("can't figure it out")

}
