package docuphase

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestDate_UnmarshalJSON(t *testing.T) {
	type fields struct {
		UnmarshalledTime Date
		ExpectedTime     time.Time
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"Proper String",
			fields{Date{}, time.Date(2006, 01, 26, 0, 0, 0, 0, time.UTC)},
			args{[]byte("01/26/2006")},
			false,
		},
		{
			"Proper String with quotes",
			fields{Date{}, time.Date(2006, 01, 26, 0, 0, 0, 0, time.UTC)},
			args{[]byte("\"01/26/2006\"")},
			false,
		},
		{
			"Improper date String with quotes",
			fields{Date{}, time.Date(2006, 01, 26, 0, 0, 0, 0, time.UTC)},
			args{[]byte("\"01/26/206\"")},
			true,
		},
		{
			"Improper date String with quote in the middle",
			fields{Date{}, time.Date(2006, 01, 26, 0, 0, 0, 0, time.UTC)},
			args{[]byte("\"01/26/\"2006\"")},
			true,
		},
		{
			"empty date String with quotes",
			fields{Date{}, time.Time{}},
			args{[]byte("\"\"")},
			false,
		},
		{
			"wrong date String but still works but is an error because it is wrong",
			fields{Date{}, time.Date(2004, 01, 26, 0, 0, 0, 0, time.UTC)},
			args{[]byte("\"01/26/2006\"")},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &tt.fields.UnmarshalledTime

			if err := d.UnmarshalJSON(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.fields.UnmarshalledTime.IsZero() {
				return
			}

			wy, wm, wd := tt.fields.ExpectedTime.Date()
			ry, rm, rd := tt.fields.UnmarshalledTime.Date()

			fmt.Println(wy, wm, wd)
			fmt.Println(ry, rm, rd)

			// if any part of the date
			if wy != ry || wm != rm || wd != rd {
				if tt.wantErr {
					t.Errorf("UnmarsalJSON times not the same")
				}
			}
		})
	}
}

// func TestDate_MarshalJSON(t *testing.T) {
// 	type fields struct {
// 		ExpectedTime time.ExpectedTime
// 	}
// 	tests := []struct {
// 		name    string
// 		fields  fields
// 		want    []byte
// 		wantErr bool
// 	}{
// 		{
// 			"Improper date String with quotes",
// 			fields{time.ExpectedTime{}},
// 			[]byte("\"01/26/206\""),
// 			true,
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			d := &Date{
// 				ExpectedTime: tt.fields.ExpectedTime,
// 			}
// 			got, err := d.MarshalJSON()
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("MarshalJSON() got = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func TestBool_UnmarshalJSON(t *testing.T) {
	type fields struct {
		bool     Bool
		expected Bool
	}
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"yes",
			fields{false, true},
			args{[]byte("Yes")},
			false,
		},
		{
			"Yes but smaller",
			fields{false, true},
			args{[]byte("Yes")},
			false,
		},
		{
			"No but ALL CAPS",
			fields{true, false},
			args{[]byte("NO")},
			false,
		},
		{
			"Si Señor",
			fields{false, true},
			args{[]byte("Si Señor")},
			true,
		},
		{
			"True",
			fields{false, true},
			args{[]byte("true")},
			false,
		},
		{
			"real bool true",
			fields{false, true},
			args{[]byte("1")},
			false,
		},
		{
			"real bool false with some extra quotes",
			fields{false, false},
			args{[]byte("\"0\"")},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &tt.fields.bool
			if err := b.UnmarshalJSON(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if *b != tt.fields.expected && !tt.wantErr {
				t.Errorf("UnmarshalJSON() didn't determine the value correctly, wanted %v, got %v ", tt.fields.expected, tt.fields.bool)
			}
		})
	}
}

func TestBool_MarshalJSON(t *testing.T) {
	type fields struct {
		bool Bool
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{"Marshall",
			fields{Bool(true)},
			[]byte("\"true\""),
			false,
		},
		{"Marshall",
			fields{Bool(false)},
			[]byte("\"false\""),
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.bool.MarshalJSON()
			if (err != nil) != tt.wantErr {
				t.Errorf("MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MarshalJSON() got = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}
