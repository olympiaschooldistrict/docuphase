package docuphase

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// DepartmentResponse response for GetDepartments
type DepartmentResponse struct {
	Departments  Departments `json:"departments"`
	HasError     bool        `json:"hasError"`
	ErrorMessage string      `json:"errorMessage"`
}

// Departments slice of departments in docuphase
type Departments []struct {
	RealName string `json:"realName"`
	ArbName  string `json:"arbName"`
}

// GetDepartments get departments
func (client *Client) GetDepartments() (deps Departments, err error) {

	req, err := client.NewAuthRequest("/departments", "GET", nil)
	if err != nil {
		return
	}

	resp, err := client.c.Do(req)
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		return deps, fmt.Errorf("http error %v", resp.StatusCode)
	}

	dr := DepartmentResponse{}

	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&dr)
	if err != nil {
		return nil, err
	}
	if dr.HasError {
		return nil, fmt.Errorf("error getting Departments: %v", dr.ErrorMessage)
	}

	if len(dr.Departments) == 1 {
		client.DefaultDepartment = dr.Departments[0].RealName
	}

	return dr.Departments, nil
}
